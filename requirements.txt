netsquid>=1.0.0,<2.0.0
numpy>=1.18.0,<2.0.0
scipy>=1.4.1,<2.0.0
netsquid_simulationtools>=2.0.0,<3.0.0
