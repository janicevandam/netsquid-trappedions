NetSquid-TrappedIons (3.1.0)
================================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

Code for simulating trapped-ion devices in quantum networks using NetSquid.

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------


How to build the docs:

First build the docs by:

```bash
make build-docs
```

This will first install any required dependencies and build the html files.

To open the built docs (using 'open' or 'xdg-open'), do:

```bash
make open-docs
```

To both build the html files and open them, do:
```bash
make docs
```

The most recent documentation of the master branch can be found [here](https://docs.netsquid.org/snippets/netsquid-trappedions/).

Usage
-----

See [module](https://docs.netsquid.org/snippets/netsquid-trappedions/ion_trap.html) for how to set up an ion trap.
See [module](https://docs.netsquid.org/snippets/netsquid-trappedions/programs.html) for some quantum programs that can be executed on an ion trap.

Contact
-------

For questions, please contact the maintainer: Guus Avis (g.avis@tudelft.nl).

Contributors
------------

- Guus Avis

License
-------

The NetSquid-TrappedIons has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
